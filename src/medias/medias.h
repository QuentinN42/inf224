#pragma once
#include<string>
#include<iostream>
#include<map>

#include "multimedia/multimedia.h"
#include "group/group.h"

enum Types {
    PHOTO,
    VIDEO
};


class Medias {
    private:
        map<string, Multimedia*> medias{};
        map<string, Group*> groups{};
    public:
        Medias();
        ~Medias();
        void newMedia(string path);
        void newMedia(string name, string path);
        void newGroup(string name);
        void bind(string groupName, string mediaName);
        void print(string name, ostream& stream);
        void open(string name);
        void list(ostream& stream);
};

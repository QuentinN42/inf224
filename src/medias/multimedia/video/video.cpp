#include<string>
#include<iostream>

#include "video.h"

using namespace std;

Video::Video() : Multimedia() {
    duration = 0;
}

Video::Video(string name, string path) : Multimedia(move(name), move(path)) {
    duration = 0;
}

Video::Video(string name, string path, int duration) : Multimedia(move(name), move(path)) {
    this->duration = duration;
}

Video::~Video() {}

const int Video::getDuration() {
    return duration;
}

void Video::setDuration(int duration) {
    this->duration = duration;
}

const void Video::print(ostream& stream) {
    stream
        << "Video('"
        << getName()
        << "','"
        << getPath()
        << "',"
        << getDuration()
        << ")"
        << endl;
}

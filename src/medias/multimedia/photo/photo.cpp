#include<string>
#include<iostream>

#include "photo.h"

using namespace std;

Photo::Photo() : Multimedia() {
    latitude = 0;
    longitude = 0;
}

Photo::Photo(string name, string path) : Multimedia(move(name), move(path)) {
    latitude = 0;
    longitude = 0;
}

Photo::Photo(string name, string path, double latitude, double longitude) : Multimedia(move(name), move(path)) {
    this->latitude = latitude;
    this->longitude = longitude;
}

Photo::~Photo() {}

const double Photo::getLatitude() {
    return latitude;
}

void Photo::setLatitude(double latitude) {
    this->latitude = latitude;
}

const double Photo::getLongitude() {
    return longitude;
}

void Photo::setLongitude(double longitude) {
    this->longitude = longitude;
}

const void Photo::print(ostream& stream) {
    stream
        << "Photo('"
        << getName()
        << "','"
        << getPath()
        << "',"
        << getLatitude()
        << ","
        << getLongitude()
        << ")"
        << endl;
}

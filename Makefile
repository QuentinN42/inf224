MAKEFLAGS += -s
PROG = out/myprog
SOURCES = $(shell find src -name '*.cpp')
OBJETS = $(patsubst src/%.cpp,out/%.o,$(SOURCES))
CXX = c++
CXXFLAGS = -std=c++11 -Wall -g
LDFLAGS = 
LDLIBS= -lpthread
.PHONY: all run clean clean-all tar server client


all: ${PROG}

run: client server

server: ${PROG}
	./${PROG}

client:
	firefox site/index.html

out:
	mkdir out

${PROG}: out out/dependencies ${OBJETS}
	${CXX} -o $@ ${CXXFLAGS} ${LDFLAGS} ${OBJETS} ${LDLIBS}

clean:
	rm -rfd out

clean-all: clean
	git clean -xdf

tar:
	tar cvf ${PROG}.tar.gz ${SOURCES}

out/dependencies: out
	${CXX} ${CXXFLAGS} -MM ${SOURCES} | sed -e 's/^/out\//g' > out/dependencies

out/%.o: src/%.cpp
	mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $<

-include out/dependencies

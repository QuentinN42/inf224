#pragma once
#include<string>
#include<iostream>

using namespace std;

class Multimedia {
    private:
        string name;
        string path;
    public:
        Multimedia();
        Multimedia(string name, string path);
        ~Multimedia();
        const string getName();
        void setName(string name);
        const string getPath();
        void setPath(string path);
        const virtual void print(ostream& stream);
        const virtual void open();
};

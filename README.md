# INF224

[Site du TP](https://perso.telecom-paristech.fr/elc/cpp/TP-C++.html).

## Setup

Le serveur peut etre lancé avec la commande suivante:

```bash
make server
```

Le client peut etre lancé avec la commande suivante:

```bash
make client
```

Les deux commandes peuvent etre lancées en même temps (peut ne pas marcher si firefox est bloquant).

```bash
make run
```

## Utilisation

### Serveur

Le serveur a ete code en C++ avec le serveur TCP.
Afin de simplifier la communication, le serveur utilise un protocole de communication simple: HTTP.

On peut donc se passer de telecomande et directement faire des requettes cUrl au serveur.

### Telecomande

La telecomande a ete code en web pour etre utilisable sur n'importe quel navigateur.
Elle supporte les commandes suivantes:

- `list` : liste les fichiers et groupes
- `open/<x>` : ouvre le fichier ou groupe `<x>`
- `print/<x>` : imprime le fichier ou groupe `<x>`

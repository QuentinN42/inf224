#pragma once
#include<string>
#include<iostream>
#include<vector>

#include "../video/video.h"

using namespace std;

class Film : public Video
{
    private:
        vector<int> duration;
    public:
        Film();
        Film(string name, string path);
        Film(string name, string path, vector<int> duration);
        ~Film();
        void setDuration(vector<int> duration[]);
        const vector<int> getDuration();
        const void virtual print(ostream& s);
};

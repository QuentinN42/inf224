#include <iostream>
#include <cstdarg>

#include "medias.h"
#include "multimedia/photo/photo.h"
#include "multimedia/video/video.h"

using namespace std;

map<string, Types> fileTypeMapping = {
    {"avi", VIDEO},
    {"mp4", VIDEO},
    {"png", PHOTO},
    {"jepg", PHOTO}
};

void Medias::newMedia(string path) {
    // name infrer from path using regex
    string name = path.substr(path.find_last_of("/") + 1);
    this->newMedia(name, path);
}

void Medias::newMedia(string name, string path) {
    string extension = path.substr(path.find_last_of(".") + 1);
    Types type = fileTypeMapping[extension];
    switch (type) {
        case VIDEO:
            medias[name] = new Video(name, path);
            break;
        case PHOTO:
            medias[name] = new Photo(name, path);
            break;
        default:
            cout << "File type not supported" << endl;
            break;
    }
}

Medias::Medias() {}

Medias::~Medias() {
    for (auto const& x : medias) {
        delete x.second;
    }
}

void Medias::newGroup(string name) {
    groups[name] = new Group();
    groups[name]->setName(name);
}

void Medias::bind(string groupName, string mediaName) {
    groups.at(groupName)->push_back(medias.at(mediaName));
}

void Medias::print(string name, ostream& stream) {
    if (medias.find(name) != medias.end()) {
        medias.at(name)->print(stream);
    } else if (groups.find(name) != groups.end()) {
        groups.at(name)->print(stream);
    } else {
        stream << "Media or group not found" << endl;
    }
}

void Medias::open(string name) {
    if (medias.find(name) != medias.end()) {
        medias.at(name)->open();
    } else if (groups.find(name) != groups.end()) {
        groups.at(name)->open();
    } else {
        cout << "Media or group not found" << endl;
    }
}

void Medias::list(ostream& stream) {
    for (auto const& x : medias) {
        stream << x.first << endl;
    }
    for (auto const& x : groups) {
        stream << x.first << endl;
    }
}

#pragma once
#include<string>
#include<iostream>

#include "../multimedia.h"

using namespace std;

class Photo : public Multimedia
{
    private:
        double latitude;
        double longitude;
    public:
        Photo();
        Photo(string name, string path);
        Photo(string name, string path, double latitude, double longitude);
        ~Photo();
        const double getLatitude();
        void setLatitude(double latitude);
        const double getLongitude();
        void setLongitude(double longitude);
        const virtual void print(ostream& stream);
};

#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include "./server/tcpserver.h"

#include "medias/medias.h"

using namespace std;

void licence() {
    cerr << "  GNU AFFERO GENERAL PUBLIC LICENSE" << endl;
    cerr << "    Version 3, 19 November 2007" << endl;
}

const int PORT = 3333;


int main(int argc, char* argv[])
{
    licence();
    Medias * medias = new Medias();
    medias->newMedia("./assets/video.mp4");
    medias->newMedia("namedLogo", "./assets/logo.png");
    medias->newGroup("group1");
    medias->bind("group1", "video.mp4");
    

    auto* server =
    new TCPServer( [&](string const& request, string& response) {
        // close the connexion at the end of the HTTP request
        if (request == ""){
            return false;
        }

        // if request does not start with GET, continue
        if (request.substr(0, 4) != "GET "){
            return true;
        }

        // Retrieve the querry from the GET request
        string querry = request.substr(5);
        querry = querry.substr(0, querry.find(" HTTP/"));

        stringstream stream;
        if (querry.substr(0, 6) == "print/"){
            string name = querry.substr(6);
            cout << "find : " << name << endl;
            medias->print(name, stream);
        }
        else if (querry.substr(0, 5) == "open/"){
            string name = querry.substr(5);
            cout << "open : " << name << endl;
            medias->open(name);
            stream << "Ok" << endl;
        } else {
            cout << "list all" << endl;
            medias->list(stream);
        }

        string data = stream.str();

        stringstream respstream;
        respstream << "HTTP/1.1 200 OK" << endl;
        respstream << "Content-Length: " << data.length() + 2 << endl;
        respstream << "Content-Type: text/raw" << endl;
        respstream << "Access-Control-Allow-Origin: *" << endl;
        respstream << "Connection: Closed" << endl;
        respstream << endl;
        respstream << data << endl;

        response = respstream.str();

        return true;
    });

    cout << "Server starting on http://0.0.0.0:" << PORT << "/" << endl;
    int status = server->run(PORT);

    return status < 0;
}

# Questions

## 4

### Comment appelle-t'on ce type de méthode et comment faut-il les déclarer ?

Ici on utilisera `xdg-open` ([cf. man](https://linux.die.net/man/1/xdg-open)) pour ouvrir le programe definit par l'utilisateur (sans lui imposer notre solution).

Sinon pour repondre a la question, c'est une methode abstraite :

    Une méthode abstraite est une méthode dont le code n'est pas implémenté dans la classe concernée. Le code sera produit dans les classes spécialisées. Une classe possédant au moins une méthode abstraite est obligatoirement abstraite. La méthode apparait en italique. 

### Si vous avez fait ce qui précède comme demandé, il ne sera plus possible d'instancer des objets de la classe de base. *Pourquoi ?*

Il est donc impossible d'instancier une classe de base car elle contient des methodes abstraites, la classe est donc abstraite.

## 5

### Quelle est la propriété caractéristique de l'orienté objet qui permet de faire cela ?

C'est le polymorphysme:

    En informatique, le polymorphisme est le concept consistant à fournir une interface unique à des entités pouvant avoir différents types.

### Qu'est-il spécifiquement nécessaire de faire dans le cas du C++ ?

Il faut definir a l'avance la taille du tableau.

### Quel est le type des éléments du tableau : le tableau doit-il contenir des objets ou des pointeurs vers ces objets ? Pourquoi ? Comparer à Java

Le tableau a pour type `vector<Multimedia*>`, il contient des pointeurs. En Java c'est la meme chose mais caché.

## 7

### Parmi les classes précédemment écrites quelles sont celles qu'il faut modifier afin qu'il n'y ait pas de fuite mémoire quand on détruit les objets ?

Toutes celles qui utilisent le Heap, soit uniquement `Film` (car utilisation de tableau).

## 10

### Les méthodes précédentes permettent d'assurer la cohérence de la base de données car quand on crée un objet on l'ajoute à la table adéquate. Par contre, ce ne sera pas le cas si on crée un objet directement avec *new* (il n'appartiendra à aucune table). Comment peut-on l'interdire, afin que seule la classe servant à manipuler les objets puisse en créer de nouveaux ?

Il faudrait rendre le constructeur private et accepter sa contruction via friend.

#include<string>
#include<iostream>
#include<list>
#include<vector>

#include "group.h"

using namespace std;

void Group::setName(string name)
{
    this->name = name;
}

const string Group::getName()
{
    return this->name;
}

const void Group::print(ostream& s)
{
    s << "# " << this->name << endl;
    for (auto it = this->begin(); it != this->end(); it++)
    {
        s << "- ";
        (*it)->print(s);
    }
}

Group* newGroup(string name, vector<Multimedia*> medias)
{
    Group* group = new Group();
    group->setName(name);
    for (auto it = medias.begin(); it != medias.end(); it++)
    {
        group->push_back(*it);
    }
    return group;
}

const void Group::open()
{
    for (auto it = this->begin(); it != this->end(); it++)
    {
        (*it)->open();
    }
}

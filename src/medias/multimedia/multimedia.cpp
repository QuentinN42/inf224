#include<string>
#include<iostream>

#include "multimedia.h"

using namespace std;

Multimedia::Multimedia() {
    name = "";
    path = "";
}

Multimedia::Multimedia(string name, string path) {
    this->name = name;
    this->path = path;
}

Multimedia::~Multimedia() {}


const string Multimedia::getName() {
    return name;
}

void Multimedia::setName(string name) {
    this->name = name;
}

const string Multimedia::getPath() {
    return path;
}

void Multimedia::setPath(string path) {
    this->path = path;
}

const void Multimedia::print(ostream& stream) {
    stream
        << "Multimedia('"
        << getName()
        << "','"
        << getPath()
        << "')"
        << endl;
}

const void Multimedia::open() {
    cout << "Opening multimedia file '" << path << "'..." << endl;
    system(("xdg-open " + path + " &").c_str());
}

#pragma once
#include<string>
#include<iostream>

#include "../multimedia.h"

using namespace std;

class Video : public Multimedia
{
    private:
        int duration;
    public:
        Video();
        Video(string name, string path);
        Video(string name, string path, int duration);
        ~Video();
        const int getDuration();
        void setDuration(int duration);
        const virtual void print(ostream& stream);
};

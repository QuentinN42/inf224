#include<string>
#include<iostream>
#include<vector>

#include "film.h"

using namespace std;

Film::Film() : Video()
{
    this->duration = vector<int>();
}

Film::Film(string name, string path) : Video(name, path)
{
    this->duration = vector<int>();
}

Film::Film(string name, string path, vector<int> duration) : Video(name, path)
{
    this->duration = duration;
}

Film::~Film()
{
    this->duration.clear();
}

void Film::setDuration(vector<int> duration[])
{
    this->duration.assign(duration->begin(), duration->end());
}

const vector<int> Film::getDuration()
{
    return this->duration;
}

const void Film::print(ostream& s)
{
    string durationString = "";
    for (unsigned int i = 0; i < this->duration.size(); i++)
    {
        durationString += to_string(this->duration[i]);
        if (i != this->duration.size() - 1)
        {
            durationString += ", ";
        }
    }
    s
        << "Film('"
        << getName()
        << "','"
        << getPath()
        << "',{"
        << durationString
        << "})"
        << endl;
}

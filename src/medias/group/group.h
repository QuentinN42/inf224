#pragma once
#include<string>
#include<iostream>
#include<list>
#include<vector>

#include "../multimedia/multimedia.h"

using namespace std;

class Group : public list<Multimedia*>
{
    private:
        string name{};
    public:
        void setName(string name);
        const string getName();
        const void virtual print(ostream& s);
        const virtual void open();
};

Group* newGroup(string name, vector<Multimedia*> medias);

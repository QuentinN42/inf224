function go() {
  let action = document.getElementById("action").value;
  let url = "http://localhost:3333/" + action;
  if (action != "list") {
    url += "/" + document.getElementById("end").value;
  }
  let command = "curl " + url;
  document.getElementById("req").innerText = command;
  fetch(url)
    .then((data) => {
      return data.text();
    })
    .then((text) => {
      document.getElementById("result").innerText = text;
    })
    .catch(console.error);
}

function copy(x) {
  window.getSelection().selectAllChildren(x);
  document.execCommand("copy");
}
